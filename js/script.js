class Characters {
    constructor(urlArray) {
        if (Array.isArray(urlArray)) {
            this.requestsArray = urlArray.map(url => fetch(url));
        }
    }

    async loadCharactersInfo() {
        try {
            const responses = await Promise.all(this.requestsArray);
            const jsonData = await Promise.all(responses.map(response => response.json()));
            const charNames = jsonData.map(data => data.name)
            return charNames.join(", ");
        } catch (e) {
            console.error(e.message);
        }
    }
}


class Films {
    constructor(url, domElemToAttach) {
        this.url = url;
        this.domElemToAttach = domElemToAttach;
    }

    async createFilmsList() {
        try {
            const request = await fetch(this.url);
            const jsonData = await request.json();
            for (const filmData of jsonData) {
                this.domElemToAttach.append(this._render(filmData))
            }
        } catch (e) {
            console.error(e.message)
        }
    }

    _render(filmsData) {
        const listOfFilms = document.createElement("ul");
        const filmId = document.createElement("li");
        filmId.innerHTML = "Film ID: " + filmsData["id"];
        listOfFilms.append(filmId);
        const filmName = document.createElement("li")
        filmName.innerHTML += "Film name: " + filmsData["name"];
        listOfFilms.append(filmName);
        const filmCharacters = document.createElement("li");
        filmCharacters.innerHTML = `<div class="loader"></div>`;
        this._createCharsList(filmCharacters, filmsData);
        listOfFilms.append(filmCharacters);
        const filmCrawl = document.createElement("li");
        filmCrawl.innerHTML += "Film opening crawl: " + filmsData["openingCrawl"];
        listOfFilms.append(filmCrawl);
        return listOfFilms;
    }

    _createCharsList(elemToAppend, film) {
        const characters = new Characters(film["characters"])
        characters.loadCharactersInfo().then(data => {
            elemToAppend.innerHTML = "Characters of film: " + data;
        })
    }
}


const films = new Films("https://ajax.test-danit.com/api/swapi/films", document.querySelector("#root"));
films.createFilmsList();
